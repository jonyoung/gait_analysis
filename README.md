# Wireless Automated Gait Analysis

Machine learning of gait measurements from wireless sensors to classify normal from affected patients. 

## Prerequisites

Software and libraries used (with versions):

* Python (v3.6)
  * imbalanced-learn (v0.3.3)
  * Numpy (v1.14)
  * Pandas (v0.20.3)
  * scikit-learn (v0.19.1)
  * JupyterLab (v0.27.0)

## License
[Apache License 2.0](LICENSE)