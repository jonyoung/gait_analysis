#!/usr/bin/env python

"""
pca3d.py: Plot 3D PCA of the wireless gait sensor data
"""

__author__  = 'Jonathan Young'
__email__   = 'jonathan.h.young@ttuhsc.edu'
__license__ = 'Apache 2.0'
__version__ = '0.1'

import matplotlib.pyplot as plt
import numpy as np
import os.path
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D
from sklearn.decomposition import PCA


datafile = os.path.join('..', 'data', 'processed', 'combined_cond_ranges.csv')
data = pd.read_csv(datafile)
X = data.iloc[:, 1:7].values
y = data.iloc[:, 7].values
target_names = ['Normal', 'Abnormal']
X_ctr = X - np.mean(X, axis=0)
pca = PCA(n_components=3)
X_r = pca.fit(X_ctr).transform(X_ctr)
print('Proportion of variance explained: %s' 
        %str(pca.explained_variance_ratio_))
colors = ['lightblue', 'darkorange']
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
for color, i, target_name in zip(colors, [0, 1], target_names):
    ax.scatter(X_r[y==i, 0], X_r[y==i, 1], X_r[y==i, 2], c=color, 
            alpha=0.8, label=target_name)
ax.set_xlabel('PC1', fontsize='x-large')
ax.set_ylabel('PC2', fontsize='x-large')
ax.set_zlabel('PC3', fontsize='x-large')
for t in ax.xaxis.get_major_ticks(): t.label.set_fontsize(12)
for t in ax.yaxis.get_major_ticks(): t.label.set_fontsize(12)
for t in ax.zaxis.get_major_ticks(): t.label.set_fontsize(12)
ax.legend(fontsize='x-large')
plt.show()

