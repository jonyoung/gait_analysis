#!/usr/bin/env python

"""
neuralnet.py: Train neural net classifier

Classify normal vs. abnormal gait from wireless sensor data
"""

__author__  = 'Jonathan H. Young'
__email__   = 'jonathan.h.young@ttuhsc.edu'
__license__ = 'Apache'
__version__ = '0.1'

"""
References:
Geron, Aurelien. Hands-On Machine Learning with Scikit-Learn and TensorFlow.
O'Reilly Media, 2017.
"""

import numpy as np
import os.path
import pandas as pd
import tensorflow as tf
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split


# Read in and setup training/test data
datafile = os.path.join('..', 'data', 'processed', 'combined_cond_ranges.csv')
data = pd.read_csv(datafile)
data_X = data.iloc[:, 1:7].values.astype(np.float32)
data_y = data.iloc[:, 7].values
X_train, X_test, y_train, y_test = train_test_split(data_X, data_y,
        test_size=0.2, random_state=42)

# Set network (hyper)parameters
n_inputs = 6
n_hidden = 4
n_outputs = 1
learning_rate = 0.001
n_epochs = 2
##batch_size = 
##n_batches = 

# Setup TensorFlow graph
X = tf.placeholder(tf.float32, shape=(None, n_inputs), name='X')
y = tf.placeholder(tf.int64, shape=(None), name='y')
with tf.name_scope('nn'):
    hidden = tf.layers.dense(X, n_hidden, name='hidden',
            activation=tf.nn.sigmoid)
    logits = tf.layers.dense(hidden, n_outputs, name='outputs')
with tf.name_scope('loss'):
    xentropy = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=y, 
            logits=logits)
    loss = tf.reduce_mean(xentropy, name='loss')
with tf.name_scope('train'):
    optimizer = tf.train.GradientDescentOptimizer(learning_rate)
    training_op = optimizer.minimize(loss)
init = tf.global_variables_initializer()

# Train/Test
with tf.Session() as sess:
    init.run()
    for epoch in range(n_epochs):
        _, ans = sess.run([training_op, logits], feed_dict={X: X_train, y: y_train})
        print(ans)

